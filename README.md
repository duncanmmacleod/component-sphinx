# Sphinx Documentation CI/CD Components

This project provides [CI/CD components](https://git.ligo.org/help/ci/components/index.html)
to help configure a [GitLab CI/CD](https://git.ligo.org/help/ci/index.html) pipeline
to enable building documentation pages with [Sphinx](https://www.sphinx-doc.org).
